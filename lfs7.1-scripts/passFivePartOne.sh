#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#



touch /var/run/utmp /var/log/{btmp,lastlog,wtmp}
chgrp -v utmp /var/run/utmp /var/log/lastlog
chmod -v 664 /var/run/utmp /var/log/lastlog
chmod -v 600 /var/log/btmp

cd /sources
tar -xvf linux-3.2.6.tar.xz
cd linux-3.2.6
make mrproper
make headers_check
make INSTALL_HDR_PATH=dest headers_install
find dest/include \( -name .install -o -name ..install.cmd \) -delete
cp -rv dest/include/* /usr/include
cd /sources
rm -rf linux-3.2.6


cd /sources
tar -zxvf man-db-2.6.1.tar.gz
cd man-db-2.6.1
#./configure
make install 
cd /sources
rm -rf man-db-2.6.1


cd /sources
tar -jxvf glibc-2.14.1.tar.bz2
cd glibc-2.14.1
DL=$(readelf -l /bin/sh | sed -n 's@.*interpret.*/tools\(.*\)]$@\1@p')
sed -i "s|libs -o|libs -L/usr/lib -Wl,-dynamic-linker=$DL -o|" \
        scripts/test-installation.pl
unset DL
sed -i -e 's/"db1"/& \&\& $name ne "nss_test1"/' scripts/test-installation.pl
sed -i 's|@BASH@|/bin/bash|' elf/ldd.bash.in
patch -Np1 -i ../glibc-2.14.1-fixes-1.patch
patch -Np1 -i ../glibc-2.14.1-sort-1.patch
patch -Np1 -i ../glibc-2.14.1-gcc_fix-1.patch
sed -i '195,213 s/PRIVATE_FUTEX/FUTEX_CLOCK_REALTIME/' \
nptl/sysdeps/unix/sysv/linux/x86_64/pthread_rwlock_timed{rd,wr}lock.S
mkdir -v ../glibc-build
cd ../glibc-build
case `uname -m` in
  i?86) echo "CFLAGS += -march=i486 -mtune=native -O3 -pipe" > configparms ;;
esac
../glibc-2.14.1/configure --prefix=/usr \
    --disable-profile --enable-add-ons \
    --enable-kernel=2.6.25 --libexecdir=/usr/lib/glibc
make
cp -v ../glibc-2.14.1/iconvdata/gconv-modules iconvdata
make -k check 2>&1 | tee glibc-check-log
grep Error glibc-check-log
touch /etc/ld.so.conf
make install
cp -v ../glibc-2.14.1/sunrpc/rpc/*.h /usr/include/rpc
cp -v ../glibc-2.14.1/sunrpc/rpcsvc/*.h /usr/include/rpcsvc
cp -v ../glibc-2.14.1/nis/rpcsvc/*.h /usr/include/rpcsvc
mkdir -pv /usr/lib/locale
localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8
localedef -i de_DE -f ISO-8859-1 de_DE
localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro
localedef -i de_DE -f UTF-8 de_DE.UTF-8
localedef -i en_HK -f ISO-8859-1 en_HK
localedef -i en_PH -f ISO-8859-1 en_PH
localedef -i en_US -f ISO-8859-1 en_US
localedef -i en_US -f UTF-8 en_US.UTF-8
localedef -i es_MX -f ISO-8859-1 es_MX
localedef -i fa_IR -f UTF-8 fa_IR
localedef -i fr_FR -f ISO-8859-1 fr_FR
localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
localedef -i it_IT -f ISO-8859-1 it_IT
localedef -i ja_JP -f EUC-JP ja_JP
localedef -i tr_TR -f UTF-8 tr_TR.UTF-8
localedef -i zh_CN -f GB18030 zh_CN.GB18030
make localedata/install-locales

cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf
passwd: files
group: files
shadow: files
hosts: files dns
networks: files
protocols: files
services: files
ethers: files
rpc: files
# End /etc/nsswitch.conf
EOF

tzselect

cp -v --remove-destination /usr/share/zoneinfo/US/Eastern /etc/localtime

cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib
EOF


cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf
EOF
mkdir /etc/ld.so.conf.d









mv -v /tools/bin/{ld,ld-old}
mv -v /tools/$(gcc -dumpmachine)/bin/{ld,ld-old}
mv -v /tools/bin/{ld-new,ld}
ln -sv /tools/bin/ld /tools/$(gcc -dumpmachine)/bin/ld

gcc -dumpspecs | sed -e 's@/tools@@g' \
    -e '/\*startfile_prefix_spec:/{n;s@.*@/usr/lib/ @}' \
    -e '/\*cpp:/{n;s@$@ -isystem /usr/include@}' > \
    `dirname $(gcc --print-libgcc-file-name)`/specs

echo 'main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'

echo "should see [Requesting program interpreter: /lib/ld-linux.so.2]"


grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

echo -e "should see /usr/lib/crt1.o succeeded\n/usr/lib/crti.o succeeded\n/usr/lib/crtn.o succeeded"


grep -B1 '^ /usr/include' dummy.log

echo -e "should see #include <...> search start here: \n /usr/include"


grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'

echo -e "should see SEARCH_DIR(\"/tools/i686-pc-linux-gnu/lib\")\nSEARCH_DIR(\"/usr/lib\")\nSEARCH_DIR(\"/lib\");"


grep "/lib.*/libc.so.6 " dummy.log

echo "should see attempt to open /lib/libc.so.6 succeeded"


grep found dummy.log

echo "should see : found ld-linux.so.2 at /lib/ld-linux.so.2"


rm -v dummy.c a.out dummy.log
cd /sources
rm -rf glibc-2.14.1





cd /sources
tar -jxvf zlib-1.2.6.tar.bz2
cd zlib-1.2.6
./configure --prefix=/usr
make
make check
make install
mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/libz.so.1.2.6 /usr/lib/libz.so
cd /sources
rm -rf zlib-1.2.6



cd /sources
tar -zxvf file-5.10.tar.gz
cd file-5.10
./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf file-5.10




cd /sources
rm -rf binutils-build
tar -jxvf binutils-2.22.tar.bz2
cd binutils-2.22
expect -c "spawn ls"
echo "should see : spawn ls"

rm -fv etc/standards.info
sed -i.bak '/^INFO/s/standards.info //' etc/Makefile.in

sed -i "/exception_defines.h/d" ld/testsuite/ld-elf/new.cc
sed -i "s/-fvtable-gc //" ld/testsuite/ld-selective/selective.exp

mkdir -v ../binutils-build
cd ../binutils-build

../binutils-2.22/configure --prefix=/usr --enable-shared

make tooldir=/usr

make -k check

make tooldir=/usr install

cp -v ../binutils-2.22/include/libiberty.h /usr/include

cd /sources
rm -rf binutils-2.22
rm -rf binutils-build





cd /sources
tar -xvf gmp-5.0.4.tar.xz
cd gmp-5.0.4
./configure --prefix=/usr --enable-cxx --enable-mpbsd
make
make check 2>&1 | tee gmp-check-log
awk '/tests passed/{total+=$2} ; END{print total}' gmp-check-log
make install
mkdir -v /usr/share/doc/gmp-5.0.4
cp    -v doc/{isa_abi_headache,configuration} doc/*.html \
/usr/share/doc/gmp-5.0.4
cd /sources
rm -rf gmp-5.0.4




cd /sources
tar -jxvf mpfr-3.1.0.tar.bz2
cd mpfr-3.1.0
patch -Np1 -i ../mpfr-3.1.0-fixes-1.patch
./configure --prefix=/usr --enable-thread-safe \
  --docdir=/usr/share/doc/mpfr-3.1.0
make
make check
make install
make html
make install-html
cd /sources
rm -rf mpfr-3.1.0






cd /sources
tar -zxvf mpc-0.9.tar.gz
cd mpc-0.9
./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf mpc-0.9




cd /sources
tar -jxvf gcc-4.6.2.tar.bz2
cd gcc-4.6.2
sed -i 's/install_to_$(INSTALL_DEST) //' libiberty/Makefile.in
case `uname -m` in
  i?86) sed -i 's/^T_CFLAGS =$/& -fomit-frame-pointer/' \
gcc/Makefile.in ;;
esac
sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in
mkdir -v ../gcc-build
cd ../gcc-build
../gcc-4.6.2/configure --prefix=/usr \
    --libexecdir=/usr/lib --enable-shared \
    --enable-threads=posix --enable-__cxa_atexit \
    --enable-clocale=gnu --enable-languages=c,c++ \
    --disable-multilib --disable-bootstrap --with-system-zlib
make
ulimit -s 16384
make -k check
../gcc-4.6.2/contrib/test_summary
make install
ln -sv ../usr/bin/cpp /lib
ln -sv gcc /usr/bin/cc
echo 'main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'
echo "should see : [Requesting program interpreter: /lib/ld-linux.so.2]"
grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

echo -e "should see :\n/usr/lib/gcc/i686-pc-linux-gnu/4.6.2/../../../crt1.o succeeded\n/usr/lib/gcc/i686-pc-linux-gnu/4.6.2/../../../crti.o succeeded\n/usr/lib/gcc/i686-pc-linux-gnu/4.6.2/../../../crtn.o succeeded"
grep -B4 '^ /usr/include' dummy.log
echo -e "should see :\n#include <...> search starts here:\n /usr/local/include\n /usr/lib/gcc/i686-pc-linux-gnu/4.6.2/include\n /usr/lib/gcc/i686-pc-linux-gnu/4.6.2/include-fixed\n /usr/include"

grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'
echo -e "should see :\nSEARCH_DIR(\"/usr/i686-pc-linux-gnu/lib\")\nSEARCH_DIR(\"/usr/local/lib\")\nSEARCH_DIR(\"/lib\")\nSEARCH_DIR(\"/usr/lib\");"

 grep "/lib.*/libc.so.6 " dummy.log
echo -e "should see :\n attempt to open /lib/libc.so.6 succeeded"

 grep found dummy.log
echo -e "should see :  found ld-linux.so.2 at /lib/ld-linux.so.2"


 rm -v dummy.c a.out dummy.log
cd /sources
rm -rf gcc-4.6.2
rm -rf gcc-build





cd /sources
tar -jxvf sed-4.2.1.tar.bz2
cd sed-4.2.1
 ./configure --prefix=/usr --bindir=/bin --htmldir=/usr/share/doc/sed-4.2.1
make
make check
make install
make -C doc install-html
cd /sources
rm -rf sed-4.2.1







cd /sources
tar -zxvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
 patch -Np1 -i ../bzip2-1.0.6-install_docs-1.patch
 sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
make -f Makefile-libbz2_so
make clean
make
 make PREFIX=/usr install
cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat

cd /sources
rm -rf bzip2-1.0.6






cd /sources
tar -zxvf ncurses-5.9.tar.gz
cd ncurses-5.9
 ./configure --prefix=/usr --with-shared --without-debug --enable-widec
make
make install
 mv -v /usr/lib/libncursesw.so.5* /lib
 ln -sfv ../../lib/libncursesw.so.5 /usr/lib/libncursesw.so
for lib in ncurses form panel menu ; do \
    rm -vf /usr/lib/lib${lib}.so ; \
    echo "INPUT(-l${lib}w)" >/usr/lib/lib${lib}.so ; \
    ln -sfv lib${lib}w.a /usr/lib/lib${lib}.a ; \
done
ln -sfv libncurses++w.a /usr/lib/libncurses++.a
rm -vf /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" >/usr/lib/libcursesw.so
ln -sfv libncurses.so /usr/lib/libcurses.so
ln -sfv libncursesw.a /usr/lib/libcursesw.a
ln -sfv libncurses.a /usr/lib/libcurses.a

mkdir -v       /usr/share/doc/ncurses-5.9
cp -v -R doc/* /usr/share/doc/ncurses-5.9

cd /sources
rm -rf ncurses-5.9





cd /sources
tar -jxvf util-linux-2.20.1.tar.bz2
cd util-linux-2.20.1
sed -e 's@etc/adjtime@var/lib/hwclock/adjtime@g' \
    -i $(grep -rl '/etc/adjtime' .)
mkdir -pv /var/lib/hwclock
 ./configure --enable-arch --enable-partx --enable-write
make
make install
cd /sources
rm -rf util-linux-2.20.1






cd /sources
tar -zxvf psmisc-22.15.tar.gz
cd psmisc-22.15
./configure --prefix=/usr
make
make install
mv -v /usr/bin/fuser /bin
mv -v /usr/bin/killall /bin
cd /sources
rm -rf psmisc-22.15






cd /sources
tar -zxvf e2fsprogs-1.42.tar.gz
cd e2fsprogs-1.42
mkdir -v build
cd build
PKG_CONFIG=/tools/bin/true LDFLAGS="-lblkid -luuid" \
    ../configure --prefix=/usr --with-root-prefix="" \
    --enable-elf-shlibs --disable-libblkid --disable-libuuid \
    --disable-uuidd --disable-fsck
make
make check
make install
 make install-libs
 chmod -v u+w /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir \
             /usr/share/info/libext2fs.info
makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir \
             /usr/share/info/com_err.info
cd /sources
rm -rf e2fsprogs-1.42







cd /sources
tar -xvf coreutils-8.15.tar.xz
cd coreutils-8.15
case `uname -m` in
 i?86 | x86_64) patch -Np1 -i ../coreutils-8.15-uname-1.patch ;;
esac
 patch -Np1 -i ../coreutils-8.15-i18n-1.patch
./configure --prefix=/usr         \
            --libexecdir=/usr/lib \
            --enable-no-install-program=kill,uptime
make
 make NON_ROOT_USERNAME=nobody check-root
 echo "dummy:x:1000:nobody" >> /etc/group
chown -Rv nobody .
 su-tools nobody -s /bin/bash -c "make RUN_EXPENSIVE_TESTS=yes check"
 sed -i '/dummy/d' /etc/group
make install
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i s/\"1\"/\"8\"/1 /usr/share/man/man8/chroot.8
 mv -v /usr/bin/{head,sleep,nice} /bin
cd /sources
rm -rf coreutils-8.15



