#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#



cd /sources
rm -rf bash-4.2




cd /sources
tar -zxvf libtool-2.4.2.tar.gz
cd libtool-2.4.2
 ./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf libtool-2.4.2





cd /sources
tar -zxvf gdbm-1.10.tar.gz
cd gdbm-1.10
 ./configure --prefix=/usr --enable-libgdbm-compat
make
make check
make install
cd /sources
rm -rf gdbm-1.10





cd /sources
tar -zxvf inetutils-1.9.1.tar.gz
cd inetutils-1.9.1
./configure --prefix=/usr --libexecdir=/usr/sbin \
    --localstatedir=/var --disable-ifconfig \
    --disable-logger --disable-syslogd --disable-whois \
    --disable-servers
make
make check
make install
make -C doc html
make -C doc install-html docdir=/usr/share/doc/inetutils-1.9.1
mv -v /usr/bin/{hostname,ping,ping6} /bin
mv -v /usr/bin/traceroute /sbin
cd /sources
rm -rf inetutils-1.9.1





cd /sources
tar -jxvf perl-5.14.2.tar.bz2
cd perl-5.14.2
 echo "127.0.0.1 localhost $(hostname)" > /etc/hosts
 patch -Np1 -i ../perl-5.14.2-security_fix-1.patch
sed -i -e "s|BUILD_ZLIB\s*= True|BUILD_ZLIB = False|"           \
       -e "s|INCLUDE\s*= ./zlib-src|INCLUDE    = /usr/include|" \
       -e "s|LIB\s*= ./zlib-src|LIB        = /usr/lib|"         \
    cpan/Compress-Raw-Zlib/config.in
sh Configure -des -Dprefix=/usr \
                  -Dvendorprefix=/usr           \
                  -Dman1dir=/usr/share/man/man1 \
                  -Dman3dir=/usr/share/man/man3 \
                  -Dpager="/usr/bin/less -isR"  \
                  -Duseshrplib
make
make test
make install
cd /sources
rm -rf perl-5.14.2




cd /sources
tar -jxvf autoconf-2.68.tar.bz2
cd autoconf-2.68
 ./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf autoconf-2.68



cd /sources
tar -xvf automake-1.11.3.tar.xz
cd automake-1.11.3
 ./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.11.3
make
make check
make install
cd /sources
rm -rf automake-1.11.3




cd /sources
tar -zxvf diffutils-3.2.tar.gz
cd diffutils-3.2
 ./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf diffutils-3.2




cd /sources
tar -jxvf gawk-4.0.0.tar.bz2
cd gawk-4.0.0
 ./configure --prefix=/usr --libexecdir=/usr/lib
make
make check
make install
mkdir -v /usr/share/doc/gawk-4.0.0
cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} \
         /usr/share/doc/gawk-4.0.0
cd /sources
rm -rf gawk-4.0.0



cd /sources
tar -zxvf findutils-4.4.2.tar.gz
cd findutils-4.4.2
./configure --prefix=/usr --libexecdir=/usr/lib/findutils \
    --localstatedir=/var/lib/locate
make
make check
make install
mv -v /usr/bin/find /bin
sed -i 's/find:=${BINDIR}/find:=\/bin/' /usr/bin/updatedb
cd /sources
rm -rf findutils-4.4.2



cd /sources
tar -jxvf flex-2.5.35.tar.bz2
cd flex-2.5.35
 patch -Np1 -i ../flex-2.5.35-gcc44-1.patch
 ./configure --prefix=/usr
make
make check
make install
 ln -sv libfl.a /usr/lib/libl.a
cat > /usr/bin/lex << "EOF"
#!/bin/sh
# Begin /usr/bin/lex
exec /usr/bin/flex -l "$@"
# End /usr/bin/lex
EOF
chmod -v 755 /usr/bin/lex
mkdir -v /usr/share/doc/flex-2.5.35
cp    -v doc/flex.pdf \
/usr/share/doc/flex-2.5.35
cd /sources
rm -rf flex-2.5.35




cd /sources
tar -zxvf gettext-0.18.1.1.tar.gz
cd gettext-0.18.1.1
./configure --prefix=/usr \
            --docdir=/usr/share/doc/gettext-0.18.1.1
make
make check
make install

cd /sources
rm -rf gettext-0.18.1.1




cd /sources
tar -zxvf groff-1.21.tar.gz
cd groff-1.21
PAGE=letter ./configure --prefix=/usr
make
make install
ln -sv eqn /usr/bin/geqn
ln -sv tbl /usr/bin/gtbl
cd /sources
rm -rf groff-1.21



cd /sources
tar -jxvf xz-5.0.3.tar.bz2
cd xz-5.0.3
 ./configure --prefix=/usr --libdir=/lib --docdir=/usr/share/doc/xz-5.0.3
make
make check
 make pkgconfigdir=/usr/lib/pkgconfig install

cd /sources
rm -rf xz-5.0.3




cd /sources
tar -zxvf grub-1.99.tar.gz
cd grub-1.99
./configure --prefix=/usr           \
             --sysconfdir=/etc      \
             --disable-grub-emu-usb \
             --disable-efiemu       \
             --disable-werror
make
make install
cd /sources
rm -rf grub-1.99



cd /sources
tar -zxvf gzip-1.4.tar.gz
cd gzip-1.4
 ./configure --prefix=/usr --bindir=/bin
make
make check
make install
mv -v /bin/{gzexe,uncompress,zcmp,zdiff,zegrep} /usr/bin
mv -v /bin/{zfgrep,zforce,zgrep,zless,zmore,znew} /usr/bin

cd /sources
rm -rf gzip-1.4



cd /sources
tar -xvf iproute2-3.2.0.tar.xz
cd iproute2-3.2.0
sed -i '/^TARGETS/s@arpd@@g' misc/Makefile
sed -i /ARPD/d Makefile
rm man/man8/arpd.8
 sed -i -e '/netlink\//d' ip/ipl2tp.c
 make DESTDIR=
make DESTDIR= MANDIR=/usr/share/man \
     DOCDIR=/usr/share/doc/iproute2-3.2.0 install
cd /sources
rm -rf iproute2-3.2.0


cd /sources
tar -zxvf kbd-1.15.2.tar.gz
cd kbd-1.15.2
 patch -Np1 -i ../kbd-1.15.2-backspace-1.patch
 ./configure --prefix=/usr --datadir=/lib/kbd
make
make install
 mv -v /usr/bin/{kbd_mode,loadkeys,openvt,setfont} /bin
mkdir -v /usr/share/doc/kbd-1.15.2
cp -R -v doc/* \
/usr/share/doc/kbd-1.15.2
cd /sources
rm -rf kbd-1.15.2



cd /sources
tar -xvf kmod-5.tar.xz
cd kmod-5
liblzma_CFLAGS="-I/usr/include" \
liblzma_LIBS="-L/lib -llzma"    \
zlib_CFLAGS="-I/usr/include"    \
zlib_LIBS="-L/lib -lz"          \
./configure --prefix=/usr --bindir=/bin --libdir=/lib --sysconfdir=/etc \
            --with-xz     --with-zlib
make
make check
make pkgconfigdir=/usr/lib/pkgconfig install
for target in depmod insmod modinfo modprobe rmmod; do
  ln -sv ../bin/kmod /sbin/$target
done
ln -sv kmod /bin/lsmod
cd /sources
rm -rf kmod-5








cd /sources
tar -zxvf less-444.tar.gz
cd less-444
./configure --prefix=/usr --sysconfdir=/etc
make
make install
cd /sources
rm -rf less-444







cd /sources
tar -zxvf libpipeline-1.2.0.tar.gz
cd libpipeline-1.2.0
./configure CHECK_CFLAGS=-I/tools/include \
    CHECK_LIBS="-L/tools/lib -lcheck" --prefix=/usr
make
make check
make install
cd /sources
rm -rf libpipeline-1.2.0






cd /sources
tar -jxvf make-3.82.tar.bz2
cd make-3.82
./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf make-3.82






cd /sources
tar -zxvf man-db-2.6.1.tar.gz
cd man-db-2.6.1
PKG_CONFIG=/tools/bin/true \
    libpipeline_CFLAGS='' \
    libpipeline_LIBS='-lpipeline' \
    ./configure --prefix=/usr --libexecdir=/usr/lib \
    --docdir=/usr/share/doc/man-db-2.6.1 --sysconfdir=/etc \
    --disable-setuid --with-browser=/usr/bin/lynx \
    --with-vgrind=/usr/bin/vgrind --with-grap=/usr/bin/grap
make
make check
make install 
cd /sources
rm -rf man-db-2.6.1







cd /sources
tar -jxvf patch-2.6.1.tar.bz2
cd patch-2.6.1
patch -Np1 -i ../patch-2.6.1-test_fix-1.patch
./configure --prefix=/usr
make
make check
make install
cd /sources
rm -rf patch-2.6.1






cd /sources
tar -jxvf shadow-4.1.5.tar.bz2
cd shadow-4.1.5
patch -Np1 -i ../shadow-4.1.5-nscd-1.patch
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \;
sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' \
       -e 's@/var/spool/mail@/var/mail@' etc/login.defs
./configure --sysconfdir=/etc
make
make install
mv -v /usr/bin/passwd /bin
cd /sources
rm -rf shadow-4.1.5

pwconv
grpconv
sed -i 's/yes/no/' /etc/default/useradd

echo -e "\n\n\nEnter new root password : \n\n"
passwd root






cd /sources
tar -zxvf sysklogd-1.5.tar.gz
cd sysklogd-1.5

make
make BINDIR=/sbin install

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf
auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *
# End /etc/syslog.conf
EOF

cd /sources
rm -rf sysklogd-1.5






cd /sources
tar -jxvf sysvinit-2.88dsf.tar.bz2
cd sysvinit-2.88dsf
sed -i 's@Sending processes@& configured via /etc/inittab@g' \
    src/init.c
sed -i -e 's/utmpdump wall/utmpdump/' \
       -e '/= mountpoint/d' \
       -e 's/mountpoint.1 wall.1//' src/Makefile
make -C src
make -C src install
cd /sources
rm -rf sysvinit-2.88dsf





cd /sources
tar -jxvf tar-1.26.tar.bz2
cd tar-1.26
FORCE_UNSAFE_CONFIGURE=1 ./configure --prefix=/usr \
   --bindir=/bin --libexecdir=/usr/sbin
make
make check
make install
make -C doc install-html docdir=/usr/share/doc/tar-1.26
cd /sources
rm -rf tar-1.26





cd /sources
tar -zxvf texinfo-4.13a.tar.gz
cd texinfo-4.13
./configure --prefix=/usr
make
make check
make install
make TEXMF=/usr/share/texmf install-tex
cd /usr/share/info
rm -v dir
for f in *
do install-info $f dir 2>/dev/null
done
cd /sources
rm -rf texinfo-4.13





cd /sources
tar -xvf udev-181.tar.xz
cd udev-181
cp -r ../udev-config-20100128 .
tar -xvf ../udev-config-20100128.tar.bz2
install -dv /lib/{firmware,udev/devices/pts}
mknod -m0666 /lib/udev/devices/null c 1 3
BLKID_CFLAGS="-I/usr/include/blkid"  \
BLKID_LIBS="-L/lib -lblkid"          \
KMOD_CFLAGS="-I/usr/include"         \
KMOD_LIBS="-L/lib -lkmod"            \
./configure  --prefix=/usr           \
             --with-rootprefix=''    \
             --bindir=/sbin          \
             --sysconfdir=/etc       \
             --libexecdir=/lib       \
             --enable-rule_generator \
             --disable-introspection \
             --disable-keymap        \
             --disable-gudev         \
             --with-usb-ids-path=no  \
             --with-pci-ids-path=no  \
             --with-systemdsystemunitdir=no
make
make check
make install
rmdir -v /usr/share/doc/udev
cd udev-config-20100128
make install
make install-doc
cd /sources
rm -rf udev-181
rm -rf udev-config-20100128


cd /sources
tar -jxvf vim-7.3.tar.bz2
cd vim73
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
./configure --prefix=/usr --enable-multibyte
make
make test
make install
ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done
 ln -sv ../vim/vim73/doc /usr/share/doc/vim-7.3

cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc
set nocompatible
set backspace=2
syntax on
if (&term == "iterm") || (&term == "putty")
  set background=dark
endif
" End /etc/vimrc
EOF

cd /sources
rm -rf vim73




