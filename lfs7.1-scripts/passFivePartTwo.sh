#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#




cd /sources
tar -jxvf iana-etc-2.30.tar.bz2
cd iana-etc-2.30
make
make install
cd /sources
rm -rf iana-etc-2.30






cd /sources
tar -jxvf m4-1.4.16.tar.bz2
cd m4-1.4.16
./configure --prefix=/usr
make
sed -i -e '41s/ENOENT/& || errno == EINVAL/' tests/test-readlink.h
make check
make install
cd /sources
rm -rf m4-1.4.16







cd /sources
tar -jxvf bison-2.5.tar.bz2
cd bison-2.5
 ./configure --prefix=/usr
 echo '#define YYENABLE_NLS 1' >> lib/config.h
make
make check
make install

cd /sources
rm -rf bison-2.5






cd /sources
tar -zxvf procps-3.2.8.tar.gz
cd procps-3.2.8
 patch -Np1 -i ../procps-3.2.8-fix_HZ_errors-1.patch
 patch -Np1 -i ../procps-3.2.8-watch_unicode-1.patch
 sed -i -e 's@\*/module.mk@proc/module.mk ps/module.mk@' Makefile
make
make install
cd /sources
rm -rf procps-3.2.8






cd /sources
tar -xvf grep-2.10.tar.xz
cd grep-2.10
 sed -i 's/cp/#&/' tests/unibyte-bracket-expr
 ./configure --prefix=/usr --bindir=/bin
make
make check
make install
cd /sources
rm -rf grep-2.10







cd /sources
tar -zxvf readline-6.2.tar.gz
cd readline-6.2
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
 patch -Np1 -i ../readline-6.2-fixes-1.patch
 ./configure --prefix=/usr --libdir=/lib
 make SHLIB_LIBS=-lncurses
make install
 mv -v /lib/lib{readline,history}.a /usr/lib
rm -v /lib/lib{readline,history}.so
ln -sfv ../../lib/libreadline.so.6 /usr/lib/libreadline.so
ln -sfv ../../lib/libhistory.so.6 /usr/lib/libhistory.so
mkdir   -v       /usr/share/doc/readline-6.2
install -v -m644 doc/*.{ps,pdf,html,dvi} \
/usr/share/doc/readline-6.2
cd /sources
rm -rf readline-6.2



cd /sources
tar -zxvf bash-4.2.tar.gz
cd bash-4.2
 patch -Np1 -i ../bash-4.2-fixes-4.patch
./configure --prefix=/usr --bindir=/bin \
    --htmldir=/usr/share/doc/bash-4.2 --without-bash-malloc \
    --with-installed-readline
make
 chown -Rv nobody .
 su-tools nobody -s /bin/bash -c "make tests"
make install
 exec /bin/bash --login +h




#logout
