#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#



cat > /etc/fstab << "EOF"
# Begin /etc/fstab
# file system	mount-point	type		options			dump	fsck order
#
/dev/sda2	/		ext3		defaults		1	1
/dev/sda1	swap		swap		pri=1			0	0
proc		/proc		proc		nosuid,noexec,nodev	0	0
sysfs		/sys		sysfs		nosuid,noexec,nodev	0	0
devpts		/dev/pts	devpts		gid=4,mode=620		0	0
tmpfs		/run		tmpfs		defaults		0	0
devtmpfs	/dev		devtmpfs	mode=0755,nosuid	0	0

# End /etc/fstab
EOF



cd /sources
tar -xvf linux-3.2.6.tar.xz
cd linux-3.2.6
make mrproper      # removes all stale *.o files lying around
#make all
#make LANG=en_US.utf8 LC_ALL= menuconfig    
make allyesconfig
make
make modules_install
cp -v arch/x86/boot/bzImage /boot/vmlinuz-3.2.6-lfs-7.1
cp -v System.map /boot/System.map-3.2.6
cp -v .config /boot/config-3.2.6
install -d /usr/share/doc/linux-3.2.6
cp -r Documentation/* /usr/share/doc/linux-3.2.6

install -v -m755 -d /etc/modprobe.d 
cat > /etc/modprobe.d/usb.conf << "EOF" 
# Begin /etc/modprobe.d/usb.conf
install ohci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i ohci_hcd ; true
install uhci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i uhci_hcd ; true
# End /etc/modprobe.d/usb.conf
EOF

cd /sources
rm -rf linux-3.2.6






#cd /sources
#tar -zxvf grub-1.99.tar.gz
#cd grub-1.99
grub-install /dev/sda

cat > /boot/grub/grub.cfg << "EOF"
# Begin /boot/grub/grub.cfg
set default=0
set timeout=5
insmod ext3
set root=(hd0,2)
menuentry "GNU/Linux, Linux 3.2.6-lfs-7.1" {
        linux   /boot/vmlinuz-3.2.6-lfs-7.1 root=/dev/sda2 ro
}
EOF

#cd /sources
#rm -rf grub-1.99


















