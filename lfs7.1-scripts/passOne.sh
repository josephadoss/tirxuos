#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#


cd /mnt/lfs/sources
tar -jxvf binutils-2.22.tar.bz2 
mkdir binutils-build
cd binutils-build
echo "\n\n\nBuilding binutils\n\n\n"
../binutils-2.22/configure \
    --target=$LFS_TGT --prefix=/tools \
    --disable-nls --disable-werror 
echo "\n\n\nMaking binutils\n\n\n"
make 
echo "\n\n\nMake installing binutils\n\n\n"
make install 
cd /mnt/lfs/sources
rm -fr binutils-build
rm -fr binutils-2.22



cd /mnt/lfs/sources

tar -jxvf gcc-4.6.2.tar.bz2
cd gcc-4.6.2

tar -jxf ../mpfr-3.1.0.tar.bz2
mv -v mpfr-3.1.0 mpfr
tar -Jxf ../gmp-5.0.4.tar.xz
mv -v gmp-5.0.4 gmp
tar -zxf ../mpc-0.9.tar.gz
mv -v mpc-0.9 mpc

patch -Np1 -i ../gcc-4.6.2-cross_compile-1.patch

mkdir -v ../gcc-build 
cd ../gcc-build
../gcc-4.6.2/configure \
    --target=$LFS_TGT --prefix=/tools \
    --disable-nls --disable-shared --disable-multilib \
    --disable-decimal-float --disable-threads \
    --disable-libmudflap --disable-libssp \
    --disable-libgomp --disable-libquadmath \
    --disable-target-libiberty --disable-target-zlib \
    --enable-languages=c --without-ppl --without-cloog \
    --with-mpfr-include=$(pwd)/../gcc-4.6.2/mpfr/src \
    --with-mpfr-lib=$(pwd)/mpfr/src/.libs 

make 
make install 
ln -vs libgcc.a `$LFS_TGT-gcc -print-libgcc-file-name | sed 's/libgcc/&_eh/'`
cd /mnt/lfs/sources
rm -fr gcc-4.6.2
rm -fr gcc-build







cd /mnt/lfs/sources
tar -xvf linux-3.2.6.tar.xz
cd linux-3.2.6
make mrproper
make headers_check
make INSTALL_HDR_PATH=dest headers_install 
cp -rv dest/include/* /tools/include
cd /mnt/lfs/sources
rm -fr linux-3.2.6





cd /mnt/lfs/sources
tar -jxvf glibc-2.14.1.tar.bz2
cd glibc-2.14.1
patch -Np1 -i ../glibc-2.14.1-gcc_fix-1.patch
patch -Np1 -i ../glibc-2.14.1-cpuid-1.patch

mkdir -v ../glibc-build
cd ../glibc-build
case `uname -m` in 
i?86) echo "CFLAGS += -march=i486 -mtune=native" > configparms ;;
esac

../glibc-2.14.1/configure --prefix=/tools \
    --host=$LFS_TGT --build=$(../glibc-2.14.1/scripts/config.guess) \
    --disable-profile --enable-add-ons \
    --enable-kernel=2.6.25 --with-headers=/tools/include \
    libc_cv_forced_unwind=yes libc_cv_c_cleanup=yes 
make 
make install 
cd /mnt/lfs/sources
rm -fr glibc-2.14.1
rm -fr glibc-build



cd /mnt/lfs/sources
SPECS=`dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/specs
$LFS_TGT-gcc -dumpspecs | sed \
  -e 's@/lib\(64\)\?/ld@/tools&@g' \
  -e "/^\*cpp:$/{n;s,$, -isystem /tools/include,}" > $SPECS 
echo "New specs file is: $SPECS"
unset SPECS



cd
echo 'main(){}' > dummy.c
$LFS_TGT-gcc -B/tools/lib dummy.c
readelf -l a.out | grep ': /tools'
rm -v dummy.c a.out


echo "should see : [Requesting program interpreter: /tools/lib/ld-linux.so.2]"
