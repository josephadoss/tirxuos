#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#


# if configuring a network card .... at some point .... do 7.2


# ensure the same names get assigned to the same devices at every boot
for NIC in /sys/class/net/* ; do
    INTERFACE=${NIC##*/} udevadm test --action=add $NIC
done


# see what names were assigned to what devices
cat /etc/udev/rules.d/70-persistent-net.rules



# create sample eth0 file
cd /etc/sysconfig/
cat > ifconfig.eth0 << "EOF" 
ONBOOT=yes
IFACE=eth0 
SERVICE=ipv4-static 
IP=192.168.1.1 
GATEWAY=192.168.1.2 
PREFIX=24 
BROADCAST=192.168.1.255
EOF



# set up DNS
#cat > /etc/resolv.conf << "EOF"
## Begin /etc/resolv.conf
#domain <Your Domain Name>
#nameserver <IP address of your primary nameserver> nameserver <IP address of your #secondary nameserver>
## End /etc/resolv.conf
#EOF
cat > /etc/resolv.conf << "EOF"
# Begin /etc/resolv.conf
domain tirxu.org
nameserver 8.8.8.8
nameserver 8.8.4.4
# End /etc/resolv.conf
EOF



# create hosts file
#cat > /etc/hosts << "EOF"
## Begin /etc/hosts (network card version)
#127.0.0.1 localhost
#<192.168.1.1> <HOSTNAME.example.org> [alias1] [alias2 ...]
## End /etc/hosts (network card version)
#EOF

#or

#cat > /etc/hosts << "EOF"
## Begin /etc/hosts (no network card version) 127.0.0.1 <HOSTNAME.example.org> #<HOSTNAME> localhost
## End /etc/hosts (no network card version)
#EOF
cat > /etc/hosts << "EOF"
# Begin /etc/hosts (no network card version) 
127.0.0.1 desktop.tirxu.org desktop localhost
# End /etc/hosts (no network card version)
EOF



# 7.4.3.2
# if module doesn't load do something with the following line
# softdep snd-pcm post: snd-pcm-oss



# 7.4.3.3
# if unwanted module is loaded, do something with the following line
# blacklist forte



# 7.5.1
# something with cd symlinks
# udevadm test /sys/block/hdd

# sed -i -e 's/"write_cd_rules"/"write_cd_rules mode"/' \ 
# /lib/udev/rules.d/75-cd-aliases-generator.rules



# 7.5.2  ... dealing with duplicate devices ...
# i'm skipping this for now
























