#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#


#chroot $LFS /tools/bin/env -i \
#    HOME=/root TERM=$TERM PS1='\u:\w\$ ' \
#    PATH=/bin:/usr/bin:/sbin:/usr/sbin \
#    /tools/bin/bash --login

#/tools/bin/find /{,usr/}{bin,lib,sbin} -type f \
#  -exec /tools/bin/strip --strip-debug '{}' ';'




