#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#



cd /mnt/lfs/sources
tar -zxvf tcl8.5.11-src.tar.gz
cd tcl8.5.11
cd unix
./configure --prefix=/tools
make
TZ=UTC make test
make install
chmod -v u+w /tools/lib/libtcl8.5.so
make install-private-headers
ln -sv tclsh8.5 /tools/bin/tclsh
cd /mnt/lfs/sources
rm -rf tcl8.5.11



cd /mnt/lfs/sources
tar -zxvf expect5.45.tar.gz
cd expect5.45
cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure
./configure --prefix=/tools --with-tcl=/tools/lib \
  --with-tclinclude=/tools/include
make
make test
make SCRIPTS="" install
cd /mnt/lfs/sources
rm -rf expect5.45






cd /mnt/lfs/sources
tar -zxvf dejagnu-1.5.tar.gz
cd dejagnu-1.5
./configure --prefix=/tools
make install
make check
cd /mnt/lfs/sources
rm -rf dejagnu-1.5






cd /mnt/lfs/sources
tar -zxvf check-0.9.8.tar.gz
cd check-0.9.8
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf check-0.9.8






cd /mnt/lfs/sources
tar -zxvf ncurses-5.9.tar.gz
cd ncurses-5.9
./configure --prefix=/tools --with-shared \
    --without-debug --without-ada --enable-overwrite
make
make install
cd /mnt/lfs/sources
rm -rf ncurses-5.9







cd /mnt/lfs/sources
tar -zxvf bash-4.2.tar.gz
cd bash-4.2
patch -Np1 -i ../bash-4.2-fixes-4.patch
./configure --prefix=/tools --without-bash-malloc
make
make tests
make install
ln -vs bash /tools/bin/sh
cd /mnt/lfs/sources
rm -rf bash-4.2







cd /mnt/lfs/sources
tar -zxvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make
make PREFIX=/tools install
cd /mnt/lfs/sources
rm -rf bzip2-1.0.6








cd /mnt/lfs/sources
tar -xvf coreutils-8.15.tar.xz
cd coreutils-8.15
./configure --prefix=/tools --enable-install-program=hostname
make
make RUN_EXPENSIVE_TESTS=yes check
make install
cp -v src/su /tools/bin/su-tools
cd /mnt/lfs/sources
rm -rf coreutils-8.15







cd /mnt/lfs/sources
tar -zxvf diffutils-3.2.tar.gz
cd diffutils-3.2
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf diffutils-3.2








cd /mnt/lfs/sources
tar -zxvf file-5.10.tar.gz
cd file-5.10
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf file-5.10






cd /mnt/lfs/sources
tar -zxvf findutils-4.4.2.tar.gz
cd findutils-4.4.2
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf findutils-4.4.2










cd /mnt/lfs/sources
tar -jxvf gawk-4.0.0.tar.bz2
cd gawk-4.0.0
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf gawk-4.0.0





cd /mnt/lfs/sources
tar -zxvf gettext-0.18.1.1.tar.gz
cd gettext-0.18.1.1
cd gettext-tools
./configure --prefix=/tools --disable-shared
make -C gnulib-lib
make -C src msgfmt
cp -v src/msgfmt /tools/bin
cd /mnt/lfs/sources
rm -rf gettext-0.18.1.1







cd /mnt/lfs/sources
tar -xvf grep-2.10.tar.xz
cd grep-2.10
./configure --prefix=/tools \
    --disable-perl-regexp
make
make check
make install
cd /mnt/lfs/sources
rm -rf grep-2.10






cd /mnt/lfs/sources
tar -zxvf gzip-1.4.tar.gz
cd gzip-1.4
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf gzip-1.4






cd /mnt/lfs/sources
tar -jxvf m4-1.4.16.tar.bz2
cd m4-1.4.16
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf m4-1.4.16






cd /mnt/lfs/sources
tar -jxvf make-3.82.tar.bz2
cd make-3.82
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf make-3.82






cd /mnt/lfs/sources
tar -jxvf patch-2.6.1.tar.bz2
cd patch-2.6.1
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf patch-2.6.1






cd /mnt/lfs/sources
tar -jxvf perl-5.14.2.tar.bz2
cd perl-5.14.2
patch -Np1 -i ../perl-5.14.2-libc-1.patch
sh Configure -des -Dprefix=/tools
make
cp -v perl cpan/podlators/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.14.2
cp -Rv lib/* /tools/lib/perl5/5.14.2
cd /mnt/lfs/sources
rm -rf perl-5.14.2






cd /mnt/lfs/sources
tar -jxvf sed-4.2.1.tar.bz2
cd sed-4.2.1
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf sed-4.2.1





cd /mnt/lfs/sources
tar -jxvf tar-1.26.tar.bz2
cd tar-1.26
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf tar-1.26





cd /mnt/lfs/sources
tar -zxvf texinfo-4.13a.tar.gz
cd texinfo-4.13
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf texinfo-4.13








cd /mnt/lfs/sources
tar -jxvf xz-5.0.3.tar.bz2
cd xz-5.0.3
./configure --prefix=/tools
make
make check
make install
cd /mnt/lfs/sources
rm -rf xz-5.0.3




chown -R root:root $LFS/tools


