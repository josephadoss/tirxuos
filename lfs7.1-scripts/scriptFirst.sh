#!/bin/bash

#
#
#    This file is part of tirxu.
#
#    tirxu is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tirxu is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tirxu.  If not, see <http://www.gnu.org/licenses/>.
#
#

#
#	Author : Joseph Doss <josephdoss@josephdoss.com>
#	Maintainer : Joseph Doss
#
#
#


export HD=sda
export BOOT=/dev/sda2
export SWAP=/dev/sda1

fdisk /dev/$HD << "EOF"
d
1
d
2
n
p
1

+512M
n
p
2


t
1
82
w
EOF

mke2fs -jv $BOOT
mkswap $SWAP

export LFS=/mnt/lfs
mkdir -pv $LFS
mount -v -t ext3 $BOOT $LFS

/sbin/swapon -v $SWAP

mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources

wget http://tirxu.org/lfs7.0/list.txt
wget -i list.txt -P $LFS/sources

mkdir -v $LFS/tools
ln -sv $LFS/tools /

groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
#useradd -s /bin/bash -g root -m -k /dev/null lfs

passwd lfs
chown -v lfs $LFS/tools
chown -v lfs $LFS/sources

su - lfs
