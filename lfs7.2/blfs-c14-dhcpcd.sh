#!/bin/bash

tar -jxvf dhcpcd-5.6.7.tar.bz2
cd dhcpcd-5.6.7

./configure --libexecdir=/lib/dhcpcd --dbdir=/run --sysconfdir=/etc && make

make install

sed -i "s;/var/lib;/run;g" dhcpcd-hooks/50-dhcpcd-compat &&
install -v -m 644 dhcpcd-hooks/50-dhcpcd-compat /lib/dhcpcd/dhcpcd-hooks/

cd ..
rm -rf dhcpcd-5.6.7


tar -xvf blfs-bootscripts-20130324.tar.bz2
cd blfs-bootscripts-20130324/blfs/services
make install-service-dhcpcd
cd ../../../
rm -rf blfs-bootscripts-20130324


cat > /etc/sysconfig/ifconfig.eth0 << "EOF"
ONBOOT="yes"
IFACE="eth0"
SERVICE="dhcpcd"
#DHCP_START="-b -q <insert appropriate start options here>"
#DHCP_STOP="-k <insert additional stop options here>"
EOF


