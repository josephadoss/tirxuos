#!/bin/bash

tar -xvf blfs-bootscripts-20130324.tar.bz2
cd blfs-bootscripts-20130324/init.d/

make install-random

cd ../../
rm -rf blfs-bootscripts-20130324
