#!/bin/bash

tar -xvf acl-2.2.51.tar.gz
cd acl-2.2.51

sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in && INSTALL_USER=root INSTALL_GROUP=root ./configure --prefix=/usr --libdir=/lib --libexecdir=/usr/lib && make

make install install-dev install-lib && chmod -v 0755 /lib/libacl.so.1.1.0 && rm -v /lib/libacl.{a,la,so} && ln -sfv ../../lib/libacl.so.1 /usr/lib/libacl.so && sed -i "s|libdir='/lib'|libdir='/usr/lib'|" /usr/lib/libacl.la && install -v -m644 doc/*.txt /usr/share/doc/acl-2.2.51

cd ..
rm -rf acl-2.2.51
