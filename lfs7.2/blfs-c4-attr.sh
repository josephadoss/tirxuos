#!/bin/bash

tar -xvf attr-2.4.46.src.tar.gz
cd attr-2.4.46.src

sed -i -e 's|/@pkg_name@|&-@pkg_version@|' include/builddefs.in && INSTALL_USER=root INSTALL_GROUP=root ./configure --prefix=/usr --libdir=/lib --libexecdir=/usr/lib && make

make install install-dev install-lib && chmod -v 0755 /lib/libattr.so.1.1.0 && rm -v /lib/libattr.{a,la,so} && sed -i 's@/lib@/usr/lib@' /usr/lib/libattr.la && ln -sfv ../../lib/libattr.so.1 /usr/lib/libattr.so


cd ..
rm -rf attr-2.4.46.src
