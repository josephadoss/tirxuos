#!/bin/bash

tar -xvf bash-4.2.tar.gz
cd bash-4.2

patch -Np1 -i ../bash-4.2-fixes-8.patch

./configure --prefix=/tools --without-bash-malloc

make

make tests

make install

ln -vs bash /tools/bin/sh

cd ..
rm -rf bash-4.2
