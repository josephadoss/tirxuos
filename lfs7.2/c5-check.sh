#!/bin/bash

tar -xvf check-0.9.8.tar.gz
cd check-0.9.8

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf check-0.9.8
