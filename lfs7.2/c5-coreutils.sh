#!/bin/bash

tar -xvf coreutils-8.19.tar.xz
cd coreutils-8.19

./configure --prefix=/tools --enable-install-program=hostname FORCE_UNSAFE_CONFIGURE=1

make

make RUN_EXPENSIVE_TESTS=yes check

make install

cd ..
rm -rf coreutils-8.19
