#!/bin/bash

tar -xvf dejagnu-1.5.tar.gz
cd dejagnu-1.5

./configure --prefix=/tools

make install

make check


cd ..
rm -rf dejagnu-1.5
