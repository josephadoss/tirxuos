#!/bin/bash

tar -xvf file-5.11.tar.gz
cd file-5.11

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf file-5.11
