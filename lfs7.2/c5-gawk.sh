#!/bin/bash


tar -xvf gawk-4.0.1.tar.xz
cd gawk-4.0.1

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf gawk-4.0.1
