#!/bin/bash

tar -xvf gettext-0.18.1.1.tar.gz
cd gettext-0.18.1.1

sed -i -e '/gets is a/d' gettext-*/*/stdio.in.h

cd gettext-tools
EMACS="no" ./configure --prefix=/tools --disable-shared

make -C gnulib-lib
make -C src msgfmt

cp -v src/msgfmt /tools/bin

cd ..

cd ..
rm -rf gettext-0.18.1.1
