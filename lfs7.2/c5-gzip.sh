#!/bin/bash

tar -xvf gzip-1.5.tar.xz
cd gzip-1.5

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf gzip-1.5
