#!/bin/bash

tar -xjvf make-3.82.tar.bz2
cd make-3.82

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf make-3.82
