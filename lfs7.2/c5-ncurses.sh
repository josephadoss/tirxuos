#!/bin/bash

tar -xvf ncurses-5.9.tar.gz
cd ncurses-5.9

./configure --prefix=/tools --with-shared --without-debug --without-ada --enable-overwrite

make

make install

cd ..
rm -rf ncurses-5.9
