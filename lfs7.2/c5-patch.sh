#!/bin/bash

tar -xjvf patch-2.6.1.tar.bz2
cd patch-2.6.1

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf patch-2.6.1
