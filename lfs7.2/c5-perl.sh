#!/bin/bash


tar -xjvf perl-5.16.1.tar.bz2
cd perl-5.16.1

patch -Np1 -i ../perl-5.16.1-libc-2.patch

sh Configure -des -Dprefix=/tools

make

cp -v perl cpan/podlators/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.16.1
cp -Rv lib/* /tools/lib/perl5/5.16.1


cd ..
rm -rf perl-5.16.1
