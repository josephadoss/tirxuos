#!/bin/bash

tar -xjvf sed-4.2.1.tar.bz2
cd sed-4.2.1

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf sed-4.2.1
