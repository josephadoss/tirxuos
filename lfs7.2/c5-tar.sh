#!/bin/bash

tar -xjvf tar-1.26.tar.bz2
cd tar-1.26

sed -i -e '/gets is a/d' gnu/stdio.in.h

./configure --prefix=/tools FORCE_UNSAFE_CONFIGURE=1

make

make check

make install

cd ..
rm -rf tar-1.26
