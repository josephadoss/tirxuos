#!/bin/bash

tar -xvf tcl8.5.12-src.tar.gz
cd tcl8.5.12

cd unix
./configure --prefix=/tools

make

TZ=UTC make test

make install

chmod -v u+w /tools/lib/libtcl8.5.so

make install-private-headers

ln -sv tclsh8.5 /tools/bin/tclsh


cd ../..
rm -rf tcl8.5.12

