#!/bin/bash

tar -xvf texinfo-4.13a.tar.gz
cd texinfo-4.13

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf texinfo-4.13
