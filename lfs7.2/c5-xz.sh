#!/bin/bash

tar -xvf xz-5.0.4.tar.xz
cd xz-5.0.4

./configure --prefix=/tools

make

make check

make install

cd ..
rm -rf xz-5.0.4
