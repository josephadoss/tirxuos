#!/bin/bash

tar -Jxvf automake-1.12.3.tar.xz
cd automake-1.12.3

./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.12.3

make

make check

make install


cd ..
rm -rf automake-1.12.3
