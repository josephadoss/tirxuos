#!/bin/bash

tar -jxvf binutils-2.22.tar.bz2
cd binutils-2.22

expect -c "spawn ls"

echo You should see    spawn ls

rm -fv etc/standards.info
sed -i.bak '/^INFO/s/standards.info //' etc/Makefile.in

patch -Np1 -i ../binutils-2.22-build_fix-1.patch

mkdir -v ../binutils-build
cd ../binutils-build

../binutils-2.22/configure --prefix=/usr --enable-shared

make tooldir=/usr

make -k check

make tooldir=/usr install

cp -v ../binutils-2.22/include/libiberty.h /usr/include

cd ..
rm -rf binutils-2.22
