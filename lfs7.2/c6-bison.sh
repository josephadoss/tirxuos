#!/bin/bash

tar -xvf bison-2.6.2.tar.xz
cd bison-2.6.2

./configure --prefix=/usr
echo '#define YYENABLE_NLS 1' >> lib/config.h
make
make check
make install


cd ..
rm -rf bison-2.6.2
