#!/bin/bash

tar -Jxvf coreutils-8.19.tar.xz
cd coreutils-8.19

sed -i -e 's/! isatty/isatty/' -e '45i\ || errno == ENOENT' gnulib-tests/test-getlogin.c

patch -Np1 -i ../coreutils-8.19-i18n-1.patch

FORCE_UNSAFE_CONFIGURE=1 ./configure --prefix=/usr --libexecdir=/usr/lib --enable-no-install-program=kill,uptime

make

make NON_ROOT_USERNAME=nobody check-root

echo "dummy:x:1000:nobody" >> /etc/group

chown -Rv nobody .

su nobody -s /bin/bash -c "PATH=$PATH make RUN_EXPENSIVE_TESTS=yes -k check || true"

sed -i '/dummy/d' /etc/group

make install



cd ..
rm -rf coreutils-8.19
