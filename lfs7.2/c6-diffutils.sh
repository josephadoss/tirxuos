#!/bin/bash

tar -xvf diffutils-3.2.tar.gz
cd diffutils-3.2

sed -i -e '/gets is a/d' lib/stdio.in.h

./configure --prefix=/usr

make

make check

make install

cd ..
rm -rf diffutils-3.2
