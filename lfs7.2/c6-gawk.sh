#!/bin/bash

tar -xvf gawk-4.0.1.tar.xz
cd gawk-4.0.1

./configure --prefix=/usr --libexecdir=/usr/lib
make
make check
make install
mkdir -v /usr/share/doc/gawk-4.0.1
cp -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-4.0.1

cd ..
rm -rf gawk-4.0.1
