#!/bin/bash

tar -jxvf gcc-4.7.1.tar.bz2
cd gcc-4.7.1

sed -i 's/install_to_$(INSTALL_DEST) //' libiberty/Makefile.in

case `uname -m` in
i?86) sed -i 's/^T_CFLAGS =$/& -fomit-frame-pointer/' gcc/Makefile.in ;;
esac

sed -i -e /autogen/d -e /check.sh/d fixincludes/Makefile.in

mkdir -v ../gcc-build
cd ../gcc-build

../gcc-4.7.1/configure --prefix=/usr --libexecdir=/usr/lib --enable-shared --enable-threads=posix --enable-__cxa_atexit --enable-clocale=gnu --enable-languages=c,c++ --disable-multilib --disable-bootstrap --with-system-zlib

make

ulimit -s 32768

make -k check

../gcc-4.7.1/contrib/test_summary

make install

ln -sv ../usr/bin/cpp /lib

ln -sv gcc /usr/bin/cc

echo 'main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'

echo You should see      [Requesting program interpreter: /lib/ld-linux.so.2]


grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

echo You should see       /usr/lib/gcc/i686-pc-linux-gnu/4.7.1/../../../crt1.o succeeded
echo You should see       /usr/lib/gcc/i686-pc-linux-gnu/4.7.1/../../../crti.o succeeded
echo You should see       /usr/lib/gcc/i686-pc-linux-gnu/4.7.1/../../../crtn.o succeeded

grep -B4 '^ /usr/include' dummy.log

echo 'You should see       include <...> search starts here:'
echo You should see       /usr/local/include
echo You should see       /usr/lib/gcc/i686-pc-linux-gnu/4.7.1/include
echo You should see       /usr/lib/gcc/i686-pc-linux-gnu/4.7.1/include-fixed
echo You should see       /usr/include

grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'

echo You should see       SEARCH_DIR\("/usr/x86_64-unknown-linux-gnu/lib64"\)
echo You should see       SEARCH_DIR\("/usr/local/lib64"\)
echo You should see       SEARCH_DIR\("/lib64"\)
echo You should see       SEARCH_DIR\("/usr/lib64"\)
echo You should see       SEARCH_DIR\("/usr/x86_64-unknown-linux-gnu/lib"\)
echo You should see       SEARCH_DIR\("/usr/local/lib"\)
echo You should see       SEARCH_DIR\("/lib"\)
echo You should see       SEARCH_DIR\("/usr/lib"\);

grep "/lib.*/libc.so.6 " dummy.log

echo YOu should see       attempt to open /lib/libc.so.6 succeeded


grep found dummy.log

echo You should see       found ld-linux.so.2 at /lib/ld-linux.so.2

rm -v dummy.c a.out dummy.log


mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib






cd ..
rm -rf gcc-4.7.1
