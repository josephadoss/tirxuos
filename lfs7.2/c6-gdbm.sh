#!/bin/bash

tar -xvf gdbm-1.10.tar.gz
cd gdbm-1.10

./configure --prefix=/usr --enable-libgdbm-compat

make

make check

make install


cd ..
rm -rf gdbm-1.10
