#!/bin/bash

tar -xvf gettext-0.18.1.1.tar.gz
cd gettext-0.18.1.1

sed -i -e '/gets is a/d' gettext-*/*/stdio.in.h
./configure --prefix=/usr --docdir=/usr/share/doc/gettext-0.18.1.1

make
make check
make install

cd ..
rm -rf gettext-0.18.1.1
