#!/bin/bash

tar -xvf grep-2.14.tar.xz
cd grep-2.14

./configure --prefix=/usr --bindir=/bin

make

make check

make install

cd ..
rm -rf grep-2.14
