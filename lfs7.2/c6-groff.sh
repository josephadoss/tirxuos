#!/bin/bash

tar -xvf groff-1.21.tar.gz
cd groff-1.21

PAGE=letter ./configure --prefix=/usr

make

make install

ln -sv eqn /usr/bin/geqn
ln -sv tbl /usr/bin/gtbl

cd ..
rm -rf groff-1.21
