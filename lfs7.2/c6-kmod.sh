#!/bin/bash

tar -xvf kmod-9.tar.xz
cd kmod-9

patch -Np1 -i ../kmod-9-testsuite-1.patch

./configure --prefix=/usr --bindir=/bin --libdir=/lib --sysconfdir=/etc --with-xz --with-zlib

make

make check

make pkgconfigdir=/usr/lib/pkgconfig install

for target in depmod insmod modinfo modprobe rmmod; do
ln -sv ../bin/kmod /sbin/$target
done

ln -sv kmod /bin/lsmod

cd ..
rm -rf kmod-9
