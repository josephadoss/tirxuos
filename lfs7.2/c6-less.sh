#!/bin/bash

tar -xvf less-444.tar.gz
cd less-444

./configure --prefix=/usr --sysconfdir=/etc

make

make install

cd ..
rm -rf less-444
