#!/bin/bash

tar -xvf libpipeline-1.2.1.tar.gz
cd libpipeline-1.2.1

sed -i -e '/gets is a/d' gnulib/lib/stdio.in.h

PKG_CONFIG_PATH=/tools/lib/pkgconfig ./configure --prefix=/usr

make

make check

make install

cd ..
rm -rf libpipeline-1.2.1
