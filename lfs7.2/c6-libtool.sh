#!/bin/bash

tar -xvf libtool-2.4.2.tar.gz
cd libtool-2.4.2

./configure --prefix=/usr
make
make check
make install

cd ..
rm -rf libtool-2.4.2
