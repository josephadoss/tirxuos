#!/bin/bash

tar -Jxvf linux-3.5.2.tar.xz
cd linux-3.5.2

make mrproper

make headers_check
make INSTALL_HDR_PATH=dest headers_install
find dest/include \( -name .install -o -name ..install.cmd \) -delete
cp -rv dest/include/* /usr/include

cd ..
rm -rf linux-3.5.2
