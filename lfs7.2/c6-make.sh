#!/bin/bash

tar -xjvf make-3.82.tar.bz2
cd make-3.82

patch -Np1 -i ../make-3.82-upstream_fixes-2.patch

./configure --prefix=/usr

make

make check

make install

cd ..
rm -rf make-3.82
