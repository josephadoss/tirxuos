#!/bin/bash

tar -Jxvf man-pages-3.42.tar.xz
cd man-pages-3.42

make install

cd ..
rm -rf man-pages-3.42
