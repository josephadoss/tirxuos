#!/bin/bash

tar -xvf mpc-1.0.tar.gz
cd mpc-1.0

./configure --prefix=/usr

make

make check

make install



cd ..
rm -rf mpc-1.0
