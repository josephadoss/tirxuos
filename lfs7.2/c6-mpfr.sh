#!/bin/bash


tar -Jxvf mpfr-3.1.1.tar.xz
cd mpfr-3.1.1

./configure --prefix=/usr --enable-thread-safe --docdir=/usr/share/doc/mpfr-3.1.1

make

make check

make install

make html
make install-html


cd ..
rm -rf mpfr-3.1.1
