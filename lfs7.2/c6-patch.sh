#!/bin/bash

tar -xjvf patch-2.6.1.tar.bz2
cd patch-2.6.1

patch -Np1 -i ../patch-2.6.1-test_fix-1.patch

./configure --prefix=/usr

make

make -k check

make install

cd ..
rm -rf patch-2.6.1
