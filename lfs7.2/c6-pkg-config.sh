#!/bin/bash

tar -xvf pkg-config-0.27.tar.gz
cd pkg-config-0.27

./configure --prefix=/usr --with-internal-glib --docdir=/usr/share/doc/pkg-config-0.27

make

make check

make install


cd ..
rm -rf pkg-config-0.27
