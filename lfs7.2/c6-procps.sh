#!/bin/bash

tar -xvf procps-3.2.8.tar.gz
cd procps-3.2.8

patch -Np1 -i ../procps-3.2.8-fix_HZ_errors-1.patch
patch -Np1 -i ../procps-3.2.8-watch_unicode-1.patch
sed -i -e 's@\*/module.mk@proc/module.mk ps/module.mk@' Makefile
make
make install

cd ..
rm -rf procps-3.2.8
