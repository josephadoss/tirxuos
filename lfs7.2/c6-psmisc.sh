#!/bin/bash

tar -xvf psmisc-22.19.tar.gz
cd psmisc-22.19

./configure --prefix=/usr

make

make install

mv -v /usr/bin/fuser /bin
mv -v /usr/bin/killall /bin

cd ..
rm -rf psmisc-22.19
