#!/bin/bash

tar -jxvf sed-4.2.1.tar.bz2
cd sed-4.2.1

patch -Np1 -i ../sed-4.2.1-testsuite_fixes-1.patch

./configure --prefix=/usr --bindir=/bin --htmldir=/usr/share/doc/sed-4.2.1

make

make html

make check

make install

make -C doc install-html

cd ..
rm -rf sed-4.2.1
