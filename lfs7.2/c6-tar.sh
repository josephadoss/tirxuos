#!/bin/bash

tar -xjvf tar-1.26.tar.bz2
cd tar-1.26

sed -i -e '/gets is a/d' gnu/stdio.in.h

FORCE_UNSAFE_CONFIGURE=1 ./configure --prefix=/usr --bindir=/bin --libexecdir=/usr/sbin

make

make check

make install

make -C doc install-html docdir=/usr/share/doc/tar-1.26

cd ..
rm -rf tar-1.26
