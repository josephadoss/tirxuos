#!/bin/bash

tar -xvf texinfo-4.13a.tar.gz
cd texinfo-4.13

./configure --prefix=/usr

make

make check

make install

make TEXMF=/usr/share/texmf install-tex


cd ..
rm -rf texinfo-4.13

cd /usr/share/info
rm -v dir
for f in *
do install-info $f dir 2>/dev/null
done
