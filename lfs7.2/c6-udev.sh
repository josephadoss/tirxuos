#!/bin/bash


tar -xvf systemd-188.tar.xz
cd systemd-188

tar -xvf ../udev-lfs-188-3.tar.bz2

make -f udev-lfs-188/Makefile.lfs

make -f udev-lfs-188/Makefile.lfs install

bash udev-lfs-188/init-net-rules.sh

cd ..
rm -rf systemd-188
