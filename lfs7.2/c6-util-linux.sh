#!/bin/bash

tar -Jxvf util-linux-2.21.2.tar.xz
cd util-linux-2.21.2

sed -i -e 's@etc/adjtime@var/lib/hwclock/adjtime@g' $(grep -rl '/etc/adjtime' .)
mkdir -pv /var/lib/hwclock

./configure

make

make install


cd ..
rm -rf util-linux-2.21.2
