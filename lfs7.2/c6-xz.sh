#!/bin/bash

tar -xvf xz-5.0.4.tar.xz
cd xz-5.0.4

./configure --prefix=/usr --libdir=/lib --docdir=/usr/share/doc/xz-5.0.4

make

make check

make pkgconfigdir=/usr/lib/pkgconfig install

cd ..
rm -rf xz-5.0.4
