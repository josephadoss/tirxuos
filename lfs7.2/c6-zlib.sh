#!/bin/bash

tar -xjvf zlib-1.2.7.tar.bz2
cd zlib-1.2.7

./configure --prefix=/usr
make
make check
make install

mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/libz.so.1.2.7 /usr/lib/libz.so

cd ..
rm -rf zlib-1.2.7
