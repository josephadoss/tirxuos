#!/bin/bash

tar -xjvf lfs-bootscripts-20120901.tar.bz2
cd lfs-bootscripts-20120901

make install

cd ..
rm -rf lfs-bootscripts-20120901
