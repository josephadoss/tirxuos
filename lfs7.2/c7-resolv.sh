#!/bin/bash

cat > /etc/resolv.conf << "EOF"
# Begin /etc/resolv.conf
#domain <Your Domain Name>
nameserver 127.0.1.1
search home

nameserver 8.8.8.8
nameserver 8.8.4.4
# End /etc/resolv.conf
EOF
