#!/bin/bash

grub-install /dev/sda

cat > /boot/grub/grub.cfg << "EOF"
# Begin /boot/grub/grub.cfg
set default=0
set timeout=5

insmod ext3
set root=(hd0,2)

menuentry "GNU/Linux, Linux 3.5.2-lfs-7.2" {
	linux /boot/vmlinuz-3.5.2-lfs-7.2 root=/dev/sda2 ro
}
EOF
