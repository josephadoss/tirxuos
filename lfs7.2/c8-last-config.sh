#!/bin/bash

echo 7.2 > /etc/lfs-release

cat > /etc/lsb-release << "EOF"
DISTRIB_ID="Linux From Scratch"
DISTRIB_RELEASE="7.2"
DISTRIB_CODENAME="Joseph Doss josephdoss@josephdoss.com"
DISTRIB_DESCRIPTION="Linux From Scratch"
EOF
