#!/bin/bash


tar -xvf linux-3.5.2.tar.xz
cd linux-3.5.2

make mrproper      # removes all stale *.o files lying around
#make all
#make LANG=en_US.utf8 LC_ALL= menuconfig    
make defconfig
make menuconfig
make
make modules_install
cp -v arch/x86_64/boot/bzImage /boot/vmlinuz-3.5.2-lfs-7.2
cp -v System.map /boot/System.map-3.5.2
cp -v .config /boot/config-3.5.2
install -d /usr/share/doc/linux-3.5.2
cp -r Documentation/* /usr/share/doc/linux-3.5.2

install -v -m755 -d /etc/modprobe.d 
cat > /etc/modprobe.d/usb.conf << "EOF" 
# Begin /etc/modprobe.d/usb.conf
install ohci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i ohci_hcd ; true
install uhci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i uhci_hcd ; true
# End /etc/modprobe.d/usb.conf
EOF

cd ..
rm -rf linux-3.5.2
