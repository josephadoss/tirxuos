#!/bin/bash

tar -xjvf binutils-2.22.tar.bz2
cd binutils-2.22
patch -Np1 -i ../binutils-2.22-build_fix-1.patch

mkdir -v ../binutils-build
cd ../binutils-build

../binutils-2.22/configure --prefix=/tools --with-sysroot=$LFS --with-lib-path=/tools/lib --target=$LFS_TGT --disable-nls --disable-werror

make

case $(uname -m) in
 x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
esac

make install

cd ..
rm -rf binutils-2.22
rm -rf binutils-build
