#!/bin/bash

tar -Jxvf glibc-2.16.0.tar.xz
cd glibc-2.16.0

if [ ! -r /usr/include/rpc/types.h ]; then
su -c 'mkdir -p /usr/include/rpc'
su -c 'cp -v sunrpc/rpc/*.h /usr/include/rpc'
fi

sed -i 's/ -lgcc_s//' Makeconfig

mkdir -v ../glibc-build
cd ../glibc-build

../glibc-2.16.0/configure --prefix=/tools --host=$LFS_TGT --build=$(../glibc-2.16.0/scripts/config.guess) --disable-profile --enable-add-ons --enable-kernel=2.6.25 --with-headers=/tools/include libc_cv_forced_unwind=yes libc_cv_ctors_header=yes libc_cv_c_cleanup=yes


make

make install

echo 'main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep ': /tools'

echo should see [Requesting program interpreter: /tools/lib/ld-linux.so.2]

rm -v dummy.c a.out

cd ..
rm -rf glibc-2.16.0
rm -rf glibc-build
