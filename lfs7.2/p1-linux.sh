#!/bin/bash

tar -Jxvf linux-3.5.2.tar.xz
cd linux-3.5.2

make mrproper

make headers_check
make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include

cd ..
rm -rf linux-3.5.2
