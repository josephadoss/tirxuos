#!/bin/bash

tar -xjvf binutils-2.22.tar.bz2
cd binutils-2.22
patch -Np1 -i ../binutils-2.22-build_fix-1.patch

mkdir -v ../binutils-build
cd ../binutils-build

CC=$LFS_TGT-gcc \
AR=$LFS_TGT-ar  \
RANLIB=$LFS_TGT-ranlib \
../binutils-2.22/configure --prefix=/tools --disable-nls --with-lib-path=/tools/lib

make

make install

make -C ld clean
make -C ld LIB_PATH=/usr/lib:/lib
cp -v ld/ld-new /tools/bin

cd ..
rm -rf binutils-2.22
rm -rf binutils-build
