#!/bin/bash



export HD=sda
export BOOT=/dev/sda2
export SWAP=/dev/sda1

fdisk /dev/$HD << "EOF"
d
1
d
2
n
p
1

+512M
n
p
2


t
1
82
w
EOF

mke2fs -jv $BOOT
mkswap $SWAP

export LFS=/mnt/lfs
mkdir -pv $LFS
mount -v -t ext3 $BOOT $LFS

/sbin/swapon -v $SWAP
